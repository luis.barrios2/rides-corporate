import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
const Schema = mongoose.Schema;
import mongoosePages from 'mongoose-pages';
import { autoIncrement } from '@matg97/mongoose-plugin-autoinc';
// import softDeletePlugin from '../libs/MongoSoftDeletePlugin.lib';

let user = new Schema(
  {
    unique_id: Number,
    user_type: Number,
    user_type_id: { type: Schema.Types.ObjectId, default: null },
    first_name: { type: String, default: '' },
    last_name: { type: String, default: '' },
    email: { type: String, default: '' },
    email_verified: { type: Boolean, default: false },
    dni: { type: String, default: '' }, // national identity
    country_phone_code: { type: String, default: '' },
    phone: { type: String, default: '' },
    gender: { type: String, default: '' },
    token: { type: String, default: '' },
    password: { type: String, default: '' },
    picture: { type: String, default: '' },
    device_token: { type: String, default: '' },
    device_type: { type: String, default: '' },
    corporate_ids: { type: Array, default: [] },
    bio: { type: String, default: '' },
    favourite_providers: [{ type: Schema.Types.ObjectId }],
    address: { type: String, default: '' },
    zipcode: { type: String, default: '' },
    social_unique_id: { type: String, default: '' },
    social_ids: [{ type: String, default: '' }],
    login_by: { type: String, default: '' },
    device_timezone: { type: String, default: '' },
    customer_id: { type: String, default: '' },
    city: { type: String, default: '' },
    cityId: { type: Schema.Types.ObjectId, ref: 'cities' },
    is_document_uploaded: { type: Number, default: 1 },
    referred_by: { type: Schema.Types.ObjectId, default: null },
    is_referral: { type: Number, default: 0 },
    country: { type: String, default: '' },
    total_referrals: { type: Number, default: 0 },
    refferal_credit: { type: Number, default: 0 },
    corporate_wallet_limit: { type: Number, default: 0 },
    wallet: { type: Number, default: 0 },
    wallet_currency_code: { type: String, default: '' },
    is_use_wallet: { type: Number, default: 1 },
    current_trip_id: { type: Schema.Types.ObjectId, default: null },
    is_approved: { type: Number, default: 1 },
    promo_count: { type: Number, default: 0 },
    home_address: { type: String, default: '' },
    work_address: { type: String, default: '' },
    home_location: {
      type: [Number],
      index1: '2d',
    },
    work_location: {
      type: [Number],
      index1: '2d',
    },

    //card uses
    card_uses: { type: Number, default: 0 },

    //consecutive cancelation trips and time of the first allowed cancelation, bool to forbid new rides, and number of sanctions historically
    cancelation_attempts: { type: [Date], default: [] },
    forbid_new_rides: { type: Boolean, default: false },
    time_until_ride_again: { type: Date, default: Date.now },

    total_request: { type: Number, default: 0 },
    completed_request: { type: Number, default: 0 },
    img: { data: Buffer, contentType: String },
    cancelled_request: { type: Number, default: 0 },
    app_version: { type: String, default: '' },
    created_at: {
      type: Date,
      default: Date.now,
    },
    updated_at: {
      type: Date,
      default: Date.now,
    },
    referral_code: { type: String, default: '' },
    rate: { type: Number, default: 0 },
    rate_count: { type: Number, default: 0 },

    tier: { type: Number, default: 5 },

    language: { type: String, default: 'es' },

    device_ids: { type: Array, default: [] },
    full_phone: { type: String },
  },
  {
    usePushEach: true,
    virtual: true,
  },
  // @ts-ignore
  {
    strict: true,
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
);

// user.index({ unique_id: 1 }, { background: true });
// user.index({ country: 1, is_approved: 1 }, { background: true });
// user.index({ phone: 1, is_approved: 1 }, { background: true });
// user.index({ email: 1, is_approved: 1 }, { background: true });
// user.index({ social_unique_id: 1 }, { background: true });
// user.index({ updated_at: 1 }, { background: true });
// user.index({ referral_code: 1 }, { background: true });
// user.index({ country_phone_code: 1, phone: 1 }, { background: true });
// user.index(
//   { device_type: 1, unique_id: 1, device_token: 1 },
//   { background: true },
// );
// user.index({ referred_by: 1 }, { background: true });
// user.index({ first_name: 1 }, { background: true });
// user.index({ last_name: 1 }, { background: true });
// user.index({ social_ids: 1 }, { background: true });
// user.index({ 'device_ids.id': 1 }, { background: true });
// user.index({ isDeleted: 1 }, { background: true });
// user.index({ is_approved: 1, unique_id: -1 }, { background: true });

// user.plugin(mongoosePaginate);
// user.plugin(autoIncrement, {
//   model: 'user',
//   field: 'unique_id',
//   startAt: 1,
//   incrementBy: 1,
// });
// mongoosePages.skip(user);

// user.virtual('cards', {
//   ref: 'Card',
//   localField: '_id',
//   foreignField: 'user_id',
// });

// user.virtual('referred', {
//   ref: 'User',
//   localField: 'referred_by',
//   foreignField: '_id',
// });

// user.plugin(softDeletePlugin);

export default user;
