import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

// interfaces
import { User } from './interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getUserById(_id: string): Promise<User> {
    const user = await this.userModel.findOne({ _id }).exec();

    return user;
  }
}
