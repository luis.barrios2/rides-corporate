export class CreateUserDTO {
  app_version: string;
  city: string;
  country: string;
  country_phone_code: string;
  device_id: string;
  device_timezone: string;
  device_token: string;
  device_type: string;
  dni: number;
  email: string;
  first_name: string;
  last_name: string;
  login_by: string;
  password: string;
  phone: number;
  social_id: string;
  social_unique_id: number;
  user_location: {
    latitude: string;
    longitude: string;
  };
}
