import {
  Get,
  Param,
  Controller,
  HttpCode,
  Version,
  VERSION_NEUTRAL,
} from '@nestjs/common';
import { UsersService } from './users.service';

@Controller({
  path: 'users',
  // version: VERSION_NEUTRAL, // e.g. '2',
})
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('/:id')
  @Version('1')
  @HttpCode(200)
  async getById(@Param('id') id) {
    console.log('from version 1');
    const user = await this.usersService.getUserById(id);

    return {
      success: true,
      user,
    };
  }

  @Get('/:id')
  @Version('2')
  @HttpCode(200)
  async getByIdV2(@Param('id') id) {
    console.log('from version 2');
    const user = await this.usersService.getUserById(id);

    return {
      success: true,
      user,
    };
  }
}
